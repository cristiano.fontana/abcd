/*
 * (C) Copyright 2016 Cristiano Lino Fontana
 *
 * This file is part of ABCD.
 *
 * ABCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ABCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ABCD.  If not, see <http://www.gnu.org/licenses/>.
 */

// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <cstring>
#include <chrono>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <zmq.h>
#include <json/json.h>

#include "class_caen_hv.h"

#include "utilities_functions.hpp"
#include "socket_functions.hpp"
#include "typedefs.hpp"
#include "states.hpp"
#include "actions.hpp"

/******************************************************************************/
/* Generic actions                                                            */
/******************************************************************************/

void actions::generic::publish_status(status &global_status,
                                      std::string topic,
                                      Json::Value status_message)
{
    void *status_socket = global_status.status_socket;
    const unsigned long int status_msg_ID = global_status.status_msg_ID;

    status_message["module"] = "hijk";
    status_message["timestamp"] = utilities_functions::time_string();
    status_message["msg_ID"] = Json::Value::UInt64(status_msg_ID);

    socket_functions::send_json_message(status_socket, topic, status_message, 1);

    global_status.status_msg_ID += 1;
}

bool actions::generic::create_hv(status &global_status)
{
    const unsigned int model_number = global_status.config["model_number"].asUInt();

    try
    {
        CAENHV *hv_card = new CAENHV(model_number);
        global_status.hv_card = hv_card;
    }
    catch (const std::bad_alloc &)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: Unable to instantiate HV card";
        std::cout << std::endl;

        return false;
    }

    return true;
}

void actions::generic::destroy_hv(status &global_status)
{
    if (global_status.hv_card)
    {
        global_status.hv_card->Deactivate();

        delete global_status.hv_card;
    }

    global_status.hv_card = nullptr;
}

/******************************************************************************/
/* Specific actions                                                           */
/******************************************************************************/

state actions::start(status&)
{
    return states::CREATE_CONTEXT;
}

state actions::create_context(status &global_status)
{
    // Creates a ØMQ context
    void *context = zmq_ctx_new();
    if (!context)
    {
        // No errors are defined for this function
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on context creation";
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    global_status.context = context;

    return states::CREATE_SOCKETS;
}

state actions::create_sockets(status &global_status)
{
    void *context = global_status.context;

    // Creates the status socket
    void *status_socket = zmq_socket(context, ZMQ_PUB);
    if (!status_socket)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on status socket creation: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Creates the commands socket
    void *commands_socket = zmq_socket(context, ZMQ_PULL);
    if (!commands_socket)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on commands socket creation: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    global_status.status_socket = status_socket;
    global_status.commands_socket = commands_socket;

    return states::BIND_SOCKETS;
}

state actions::bind_sockets(status &global_status)
{
    std::string status_address = global_status.status_address;
    std::string commands_address = global_status.commands_address;

    // Binds the status socket to its address
    const int s = zmq_bind(global_status.status_socket, status_address.c_str());
    if (s != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on status socket binding: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Binds the socket to its address
    const int c = zmq_bind(global_status.commands_socket, commands_address.c_str());
    if (c != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on commands socket binding: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    return states::READ_CONFIG;
}

state actions::read_config(status &global_status)
{
    std::string config_file = global_status.config_file;

    std::ifstream input_file;
    input_file.open(config_file, std::ifstream::in);

    if (!input_file.good())
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: Unable to read config file: ";
        std::cout << config_file;
        std::cout << std::endl;

        return states::IO_ERROR;
    }

    Json::Value config;
    Json::Reader reader;

    const bool parse_success = reader.parse(input_file, config, false);

    if (!parse_success)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: Parse error while reading config file";
        std::cout << std::endl;

        return states::IO_ERROR;
    }

    global_status.config = config;

    return states::CREATE_HV;
}

state actions::create_hv(status &global_status)
{
    Json::Value status_message;

    status_message["type"] = "event";
    status_message["event"] = "HV initialization";

    actions::generic::publish_status(global_status, defaults_hijk_events_topic, status_message);

    const bool success = actions::generic::create_hv(global_status);

    if (success)
    {
        return states::INITIALIZE_HV;
    }
    else
    {
        return states::CONFIGURE_ERROR;
    }
}

state actions::recreate_hv(status &global_status)
{
    const bool success = actions::generic::create_hv(global_status);

    if (success)
    {
        return states::INITIALIZE_HV;
    }
    else
    {
        return states::CONFIGURE_ERROR;
    }
}

state actions::reconfigure_destroy_hv(status &global_status)
{
    actions::generic::destroy_hv(global_status);

    return states::RECREATE_HV;
}


state actions::initialize_hv(status &global_status)
{
    CAENHV *hv_card = global_status.hv_card;

    if (!hv_card)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: HV pointer unavailable";
        std::cout << std::endl;

        return states::CONFIGURE_ERROR;
    }

    unsigned int model_number = 0;

    try
    {
        model_number = global_status.config["model_number"].asUInt();
    }
    catch (...)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: Wrong format of model number; ";
        std::cout << std::endl;

        return states::CONFIGURE_ERROR;
    }

    const std::string string_connection_type(global_status.config["connection_type"].asString());

    // This is an enum class
    CAEN_Comm_ConnectionType connection_type;
    if (string_connection_type == "PCI")
    {
        connection_type = CAENComm_OpticalLink;
    }
    else if (string_connection_type == "PCIe")
    {
        connection_type = CAENComm_OpticalLink;
    }
    else if (string_connection_type == "OpticalLink")
    {
        connection_type = CAENComm_OpticalLink;
    }
    else if (string_connection_type == "USB")
    {
        connection_type = CAENComm_USB;
    }
    else
    {
        connection_type = static_cast<CAEN_Comm_ConnectionType>(global_status.connection_type);
    }

    unsigned int link_number = global_status.link_number;

    try
    {
        link_number = global_status.config["link_number"].asUInt();
    }
    catch (...)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: Wrong format of link number; ";
        std::cout << std::endl;

        return states::CONFIGURE_ERROR;
    }

    unsigned int CONET_node = global_status.CONET_node;

    try
    {
        CONET_node = global_status.config["CONET_node"].asUInt();
    }
    catch (...)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: Wrong format of CONET node; ";
        std::cout << std::endl;

        return states::CONFIGURE_ERROR;
    }

    uint32_t VME_address = global_status.VME_address;

    try
    {
        VME_address = std::stoul(global_status.config["VME_address"].asString(), nullptr, 0);
    }
    catch (...)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: Wrong format of VME address";
        std::cout << std::endl;

        return states::CONFIGURE_ERROR;
    }

    unsigned int verbosity = global_status.verbosity;

    if (hv_card->IsActive() == 1)
    {
        if (verbosity > 0)
        {
            std::cout << '[' << utilities_functions::time_string() << "] ";
            std::cout << "Card already active, deactivating it";
            std::cout << std::endl;
        }
        hv_card->Deactivate();
    }

    if (verbosity > 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "Initializing card model; ";
        std::cout << std::endl;
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "Model number: " << model_number << "; ";
        std::cout << std::endl;
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "Connection type: " << connection_type << "; ";
        std::cout << std::endl;
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "Link number: " << link_number << "; ";
        std::cout << std::endl;
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "CONET node: " << CONET_node << "; ";
        std::cout << std::endl;
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "VME address: 0x" << std::hex << VME_address << std::dec << "; ";
        std::cout << std::endl;
    }
    hv_card->InitializeModel(model_number);

    if (hv_card->IsActive() == 0)
    {
        if (verbosity > 0)
        {
            std::cout << '[' << utilities_functions::time_string() << "] ";
            std::cout << "Activating card; ";
            std::cout << std::endl;
        }
        hv_card->Activate(connection_type, link_number, CONET_node, VME_address);
    }

    if (hv_card->IsActive() == 0)
    {
        if (verbosity > 0)
        {
            std::cout << '[' << utilities_functions::time_string() << "] ";
            std::cout << "ERROR: Unable to activate card";
            std::cout << std::endl;
        }
        hv_card->Deactivate();

        return states::CONFIGURE_ERROR;
    }
    else
    {
        if (verbosity > 0)
        {
            std::cout << '[' << utilities_functions::time_string() << "] ";
            std::cout << "Card activated";
            std::cout << std::endl;
        }
    }

    if (verbosity > 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "Description: " << hv_card->GetDescr() << "; ";
        std::cout << std::endl;
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "Model: " << hv_card->GetModel() << "; ";
        std::cout << std::endl;
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "Number of channels: " << hv_card->GetChNum() << "; ";
        std::cout << std::endl;
    }

    return states::CONFIGURE_HV;
}

state actions::configure_hv(status &global_status)
{
    CAENHV *hv_card = global_status.hv_card;

    if (!hv_card)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: HV pointer unavailable";
        std::cout << std::endl;

        return states::CONFIGURE_ERROR;
    }

    for (unsigned int i = 0; i < global_status.config["channels"].size(); i++)
    {
        const Json::Value channel_config = global_status.config["channels"][i];

        const unsigned int channel = channel_config["id"].asUInt();
        const bool on = channel_config["on"].asBool();
        const bool previous_on = hv_card->IsChannelOn(channel);

        if (on)
        {
            const unsigned int voltage = channel_config["voltage"].asUInt();
            const unsigned int max_current = channel_config["max_current"].asUInt();
            const unsigned int max_voltage = channel_config["max_voltage"].asUInt();
            const unsigned int ramp_up = channel_config["ramp_up"].asUInt();
            const unsigned int ramp_down = channel_config["ramp_down"].asUInt();

            if (global_status.verbosity > 0)
            {
                std::cout << '[' << utilities_functions::time_string() << "] ";
                std::cout << "Settings for channel: " << channel << "; ";
                std::cout << std::endl;
                std::cout << '[' << utilities_functions::time_string() << "] ";
                std::cout << "Voltage: " << voltage << "; ";
                std::cout << std::endl;
                std::cout << '[' << utilities_functions::time_string() << "] ";
                std::cout << "Max current: " << max_current << "; ";
                std::cout << std::endl;
                std::cout << '[' << utilities_functions::time_string() << "] ";
                std::cout << "Max voltage: " << max_voltage << "; ";
                std::cout << std::endl;
            }

            hv_card->SetChannelVoltage(channel, voltage);
            hv_card->SetChannelCurrent(channel, max_current);
            hv_card->SetChannelSVMax(channel, max_voltage);
            hv_card->SetChannelRampUp(channel, ramp_up);
            hv_card->SetChannelRampDown(channel, ramp_down);
            hv_card->SetChannelOn(channel);
            //hv_card->SetChannelTripTime(int channel, int time);
            //hv_card->SetChannelPowerDownMode(int channel, int mode);
        }
        else
        {
            hv_card->SetChannelOff(channel);
        }

        if (on && !previous_on)
        {
            Json::Value status_message;

            status_message["type"] = "event";
            status_message["event"] = "Channel " + std::to_string(channel) + " turned on";

            actions::generic::publish_status(global_status, defaults_hijk_events_topic, status_message);
        }
        else if (!on && previous_on)
        {
            Json::Value status_message;

            status_message["type"] = "event";
            status_message["event"] = "Channel " + std::to_string(channel) + " turned off";

            actions::generic::publish_status(global_status, defaults_hijk_events_topic, status_message);
        }
    }

    return states::RECEIVE_COMMANDS;
}

state actions::receive_commands(status &global_status)
{
    void *commands_socket = global_status.commands_socket;

    Json::Value json_message = socket_functions::receive_json_message(commands_socket);

    if (json_message.isMember("command"))
    {
        CAENHV *hv_card = global_status.hv_card;

        if (!hv_card)
        {
            std::cout << '[' << utilities_functions::time_string() << "] ";
            std::cout << "ERROR: HV pointer unavailable";
            std::cout << std::endl;

            return states::CONFIGURE_ERROR;
        }

        const std::string command = json_message["command"].asString();

        if (command == std::string("reconfigure") && json_message.isMember("arguments"))
        {
            const Json::Value arguments = json_message["arguments"];

            if (arguments.isMember("config"))
            {
                const Json::Value new_config = arguments["config"];
                global_status.config = new_config;

                return states::CONFIGURE_HV;
            }
        }
        else if (command == std::string("off") && json_message.isMember("arguments"))
        {
            const Json::Value arguments = json_message["arguments"];

            if (arguments.isMember("channel"))
            {
                const unsigned int channel = arguments["channel"].asUInt();

                hv_card->SetChannelOff(channel);
            }
        }
        else if (command == std::string("on") && json_message.isMember("arguments"))
        {
            const Json::Value arguments = json_message["arguments"];

            if (arguments.isMember("channel"))
            {
                const unsigned int channel = arguments["channel"].asUInt();

                hv_card->SetChannelOn(channel);
            }
        }
        else if (command == std::string("set_voltage") && json_message.isMember("arguments"))
        {
            const Json::Value arguments = json_message["arguments"];

            if (arguments.isMember("channel") && arguments.isMember("voltage"))
            {
                const unsigned int channel = arguments["channel"].asUInt();
                const unsigned int voltage = arguments["voltage"].asUInt();

                hv_card->SetChannelVoltage(channel, voltage);
            }
        }
        else if (command == std::string("quit"))
        {
            return states::DESTROY_HV;
        }
    }

    return states::PUBLISH_STATUS;
}

state actions::publish_status(status &global_status)
{
    CAENHV *hv_card = global_status.hv_card;

    Json::Value status_message;

    status_message["config"] = global_status.config;

    status_message["hv_card"] = Json::Value();

    if (hv_card)
    {
        status_message["hv_card"]["valid_pointer"] = true;
        status_message["hv_card"]["active"] = hv_card->IsActive() ? true : false;

        status_message["hv_card"]["channels"] = Json::Value(Json::ValueType::arrayValue);

        const unsigned int channels = hv_card->GetChNum();

        for (unsigned int channel = 0; channel < channels; channel++)
        {
            Json::Value channel_info;

            std::string status_description;

            const int *status_bits = hv_card->GetChStatusArray(channel);
            for (unsigned int i=1; i<13; i++)
            {
                if (status_bits[i] != 0)
                {
                    status_description += hv_card->GetChStatusDesc(i);
                    status_description += "; ";
                }
            }

            if (status_description.length() <= 0)
            {
                status_description = "OK";
            }

            channel_info["id"] = channel;
            channel_info["on"] = hv_card->IsChannelOn(channel);
            channel_info["current"] = hv_card->GetChannelCurrent(channel);
            channel_info["voltage"] = hv_card->GetChannelVoltage(channel);
            //channel_info["temperature"] = hv_card->GetChannelTemperature(channel);
            channel_info["status"] = status_description;

            status_message["hv_card"]["channels"].append(channel_info);
        }
    }
    else
    {
        status_message["hv_card"]["valid_pointer"] = false;
    }

    actions::generic::publish_status(global_status, defaults_hijk_status_topic, status_message);

    if (hv_card)
    {
        return states::RECEIVE_COMMANDS;
    }
    else
    {
        return states::CONFIGURE_ERROR;
    }
}

state actions::destroy_hv(status &global_status)
{
    Json::Value status_message;

    status_message["type"] = "event";
    status_message["event"] = "HV deactivation";

    actions::generic::publish_status(global_status, defaults_hijk_events_topic, status_message);

    actions::generic::destroy_hv(global_status);

    return states::CLOSE_SOCKETS;
}

state actions::close_sockets(status &global_status)
{
    void *status_socket = global_status.status_socket;
    void *commands_socket = global_status.commands_socket;

    const int s = zmq_close(status_socket);
    if (s != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on status socket close: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    const int c = zmq_close(commands_socket);
    if (c != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on commands socket close: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    return states::DESTROY_CONTEXT;
}

state actions::destroy_context(status &global_status)
{
    void *context = global_status.context;

    const int c = zmq_ctx_destroy(context);
    if (c != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on context destroy: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    return states::STOP;
}

state actions::communication_error(status &global_status)
{
    Json::Value status_message;

    status_message["type"] = "error";
    status_message["error"] = "Communication error";

    actions::generic::publish_status(global_status, defaults_hijk_events_topic, status_message);

    return states::CLOSE_SOCKETS;
}

state actions::configure_error(status &global_status)
{
    Json::Value status_message;

    status_message["type"] = "error";
    status_message["error"] = "Configure error";

    actions::generic::publish_status(global_status, defaults_hijk_events_topic, status_message);

    return states::RECONFIGURE_DESTROY_HV;
}

state actions::io_error(status &global_status)
{
    Json::Value status_message;

    status_message["type"] = "error";
    status_message["error"] = "I/O error";

    actions::generic::publish_status(global_status, defaults_hijk_events_topic, status_message);

    return states::CLOSE_SOCKETS;
}

state actions::stop(status&)
{
    return states::STOP;
}
