#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "selection.h"
#include "events.h"

#define UNUSED(x) (void)(x)

/*! An initialization function for auxiliary data to be given to the
 *  select_event() function. It shall return a pointer to a block of data that
 *  contains the auxiliary data. The users must take care of cleaning up the
 *  memory.
 *
 * \return A pointer to the allocated data.
 */
void *select_init()
{
////////////////////////////////////////////////////////////////////////////////
    // In this example we will set a threshold for the qlong and for the PSD
    // parameter, so we need to allocate some memory to store the threshold value.
    double *thresholds = malloc(2 * sizeof(double));

    if (thresholds) {
        // This is going to be the energy threshold
        thresholds[0] = 1000;
        // This is going to be the PSD threshold
        thresholds[1] = 0.8;

        return (void*)thresholds;
    } else {
        return NULL;
    }
}

/*! The events selection function. It shall return a boolean value that
 *  indicated whether the event should be forwarded or not.
 *  The user shall make sure that user_data is a valid pointer.
 *  Warning: This function should be as fast as possible, as it is run
 *  on every event.
 *
 * \param samples_number The number of samples in the waveform.
 * \param samples A pointer to the samples array.
 * \param baseline_end The position from which the integrals are calculated, as provided by the user or calculared by the CFD.
 * \param timestamp The timestamp of the event.
 * \param qshort The calculated qshort value of the event, as a double.
 * \param qlong The calculated qlong value of the event, as a double.
 * \param baseline The calculatd baseline of the waveform, as a double.
 * \param channel The channel number.
 * \param PUR The Pile-Up Rejection flag (currently not calculated).
 * \param user_data Pointer to the current event, in order to allow edits by the user.
 * \param user_data The pointer to the auxiliary data.
 * \return A boolean value to select the event.
 */
bool select_event(uint32_t samples_number,
                  const uint16_t *samples,
                  size_t baseline_end,
                  uint64_t timestamp,
                  double qshort,
                  double qlong,
                  double baseline,
                  uint8_t channel,
                  uint8_t PUR,
                  struct event_PSD *event,
                  void *user_data)
{
    // These are not necessary, they are only used to avoid warnings.
    UNUSED(samples_number);
    UNUSED(samples);
    UNUSED(baseline_end);
    UNUSED(timestamp);
    UNUSED(baseline);
    UNUSED(channel);
    UNUSED(PUR);
    UNUSED(event);

    const double *thresholds = user_data;

    if (!thresholds) {
        // This is an error as the user did not allocate the user_data.
        // We will signal it by not selecting any event.
        return false;
    } else {
        const double psd = (qlong - qshort) / qlong;

        return (qlong > thresholds[0]) && (psd < thresholds[1]);
    }
}

/*! A clean-up function in which the user shall deallocate the memory
 *  allovated for the auxiliary data.
 *  The user shall make sure that user_data is a valid pointer.
 *
 * \param user_data The pointer to the auxiliary data.
 * \return An integer indetifying potential errors. A zero means no error.
 */
int select_close(void *user_data)
{
    if (user_data) {
        free(user_data);
        return 0;
    } else {
        return 1;
    }
}

