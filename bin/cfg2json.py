#! /usr/bin/env python3
#
# Changelog:
#
#   18/02/2015: First version, Cristiano Lino Fontana

import argparse
import configparser
import itertools
import json

parser = argparse.ArgumentParser(description='Convert cfg files to JSON')
parser.add_argument('input_file',
                    type = str,
                    help = 'Input file')
parser.add_argument('-o',
                    '--output_file',
                    type = str,
                    help = 'Output file',
                    default = None)

args = parser.parse_args()

config = configparser.ConfigParser()
# Keep option names as they are.
# The default behaviour is to convert them to lowercase
config.optionxform = lambda option: option

try:
    # Normally on cfg files there are no sections, but configparser expects at least one
    iterator = itertools.chain(['[DEFAULT]'], open(args.input_file))
    config.read_file(iterator)
    #config.read_file(args.input_file)

    output_dict = dict()

    for section in config:
        #print("[{}]".format(section))

        if section == 'DEFAULT':
            section_dict = output_dict
        else:
            if section not in output_dict:
                output_dict[section] = dict()

            section_dict = output_dict[section]

        for item in config[section]:
            #print(item, "=", config[section][item])
    
            tokens = item.split('.')
            value = config[section][item].strip('"')
    
            try:
                # Trying to convert a string to an int value
                value = int(value)
            except ValueError:
                pass
    
            deeper_dict = section_dict
    
            if isinstance(tokens, list):
                for token in tokens[:-1]:
                    if token not in deeper_dict:
                        deeper_dict[token] = dict()
                    deeper_dict = deeper_dict[token]
    
                deeper_dict[tokens[-1]] = value
            else:
                deeper_dict[item] = value

    try:
        with open(args.output_file, "w") as output_file:
            json.dump(output_dict, output_file, sort_keys = True, indent = 4)
    except TypeError:
        print(json.dumps(output_dict, sort_keys = True, indent = 4))

except configparser.Error as error:
    print("Parsing cfg file error: {}".format(error))
