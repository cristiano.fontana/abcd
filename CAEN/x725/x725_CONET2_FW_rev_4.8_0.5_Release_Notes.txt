
  -----------------------------------------------------------------------------

                   --- CAEN SpA - Front End R&D Division --- 

  -----------------------------------------------------------------------------


  This file is x725 FPGA default firmware revision history.
  Default firmware makes the board operate as a waveform recorder
  x725 digitizer board houses two kinds of FPGA devices that are 
  configured with a single programming file:
    - ROC FPGA is the mainboard FPGA for communication and trigger handling.
    - AMC FPGA is the mezzanine FPGA for ADC and channel memory management:
      there are eight of them, but they are loaded with the same firmware image.
       
  Release numbers are in the form X.X_Y.Y, where X.X is the motherboard 
  FPGA release, while Y.Y is the channel mezzanine FPGA release.


 
===============================================================================
Release Number: 4.8_0.5
Release Date: October 2015
===============================================================================
  
  First official release.
	  
===============================================================================
     
  PREVIOUS REVISIONS ARE CONSIDERED AS BETAs FOR INTERNAL USE 
  AND ARE NOT DOCUMENTED HERE
 	  
===============================================================================

How to get support:
-------------------

For technical support, please contact CAEN at

support.nuclear@caen.it (hardware and firmware inquiries)
support.computing@caen.it (software and libraries inquiries)
      