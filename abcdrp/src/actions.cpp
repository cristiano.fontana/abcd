/*
 * (C) Copyright 2016 Cristiano Lino Fontana
 *
 * This file is part of ABCD.
 *
 * ABCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ABCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ABCD.  If not, see <http://www.gnu.org/licenses/>.
 */

// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <cstring>
#include <cmath>
#include <chrono>
#include <unistd.h>
#include <iostream>
#include <string>
#include <thread>
#include <zmq.h>
#include <jansson.h>
//#include <digilent/waveforms/dwf.h>
#include "redpitaya/rp.h"

extern "C" {
#include "defaults.h"
#include "utilities_functions.h"
#include "socket_functions.h"
#include "jansson_socket_functions.h"
}

#include "typedefs.hpp"
#include "events.hpp"
#include "states.hpp"
#include "actions.hpp"

#define BUFFER_SIZE 32
#define POSMAX 16384 

/******************************************************************************/
/* Generic actions                                                            */
/******************************************************************************/

void actions::generic::publish_events(status &global_status)
{
    const size_t waveforms_buffer_size = global_status.waveforms_buffer.size();

    if (waveforms_buffer_size > 0)
    {
        size_t total_size = 0;

        for (auto &event: global_status.waveforms_buffer)
        {
            total_size += event.size();
        }

        std::vector<uint8_t> output_buffer(total_size);

        size_t portion = 0;

        for (auto &event: global_status.waveforms_buffer)
        {
            const size_t event_size = event.size();
            const std::vector<uint8_t> event_buffer = event.serialize();
            
            memcpy(output_buffer.data() + portion,
                   reinterpret_cast<const void*>(event_buffer.data()),
                   event_size);

            portion += event_size;
        }

        std::string topic = defaults_abcd_data_waveforms_topic;
        topic += "_v0_s";
        topic += std::to_string(total_size);

        if (global_status.verbosity > 0)
        {
            char time_buffer[BUFFER_SIZE];
            time_string(time_buffer, BUFFER_SIZE, NULL);
            std::cout << '[' << time_buffer << "] ";
            std::cout << "Sending waveforms buffer; ";
            std::cout << "Topic: " << topic << "; ";
            std::cout << "waveforms: " << waveforms_buffer_size << "; ";
            std::cout << "buffer size: " << total_size << "; ";
            std::cout << std::endl;
        }

        const bool result = send_byte_message(global_status.data_socket,
                                              topic.c_str(),
                                              output_buffer.data(),
                                              total_size,
                                              1);

        if (result == EXIT_FAILURE)
        {
            char time_buffer[BUFFER_SIZE];
            time_string(time_buffer, BUFFER_SIZE, NULL);
            std::cout << '[' << time_buffer << "] ";
            std::cout << "ERROR: ZeroMQ Error publishing events";
            std::cout << std::endl;
        }

        // Cleanup vector
        global_status.waveforms_buffer.clear();
        // Initialize vector size to its max size plus a 10%
        global_status.waveforms_buffer.reserve(global_status.events_buffer_max_size \
                                               + \
                                               global_status.events_buffer_max_size / 10);
    }
}

void actions::generic::publish_message(status &global_status,
                                      std::string topic,
                                      json_t *status_message)
{
    std::chrono::time_point<std::chrono::system_clock> last_publication = std::chrono::system_clock::now();
    global_status.last_publication = last_publication;

    void *status_socket = global_status.status_socket;
    const unsigned long int status_msg_ID = global_status.status_msg_ID;

    char time_buffer[BUFFER_SIZE];
    time_string(time_buffer, BUFFER_SIZE, NULL);

    json_object_set_new(status_message, "module", json_string("abcd"));
    json_object_set_new(status_message, "timestamp", json_string(time_buffer));
    json_object_set_new(status_message, "msg_ID", json_integer(status_msg_ID));

    char *output_buffer = json_dumps(status_message, JSON_COMPACT);

    if (!output_buffer)
    {
        char time_buffer[BUFFER_SIZE];
        time_string(time_buffer, BUFFER_SIZE, NULL);
        std::cout << '[' << time_buffer << "] ";
        std::cout << "ERROR: Unable to allocate status output buffer; ";
        std::cout << std::endl;
    }
    else
    {
        size_t total_size = strlen(output_buffer);

        topic += "_v0_s";
        topic += std::to_string(total_size);

        if (global_status.verbosity > 0)
        {
            char time_buffer[BUFFER_SIZE];
            time_string(time_buffer, BUFFER_SIZE, NULL);
            std::cout << '[' << time_buffer << "] ";
            std::cout << "Sending status message; ";
            std::cout << "Topic: " << topic << "; ";
            std::cout << "buffer size: " << total_size << "; ";
            std::cout << "message: " << output_buffer << "; ";
            std::cout << std::endl;
        }

        send_byte_message(status_socket, topic.c_str(), output_buffer, total_size, 1);

        free(output_buffer);
    }

    global_status.status_msg_ID += 1;
}

void actions::generic::stop_acquisition(status &global_status)
{
    const unsigned int verbosity = global_status.verbosity;

    // Stop acquisition
    // zpe
    std::cout << "\nSTOPPO L'ACQUISIZIONE\n\n";

    if (rp_AcqStop() != RP_OK) 
      {
	char time_buffer[BUFFER_SIZE];
	time_string(time_buffer, BUFFER_SIZE, NULL);
	std::cout << '[' << time_buffer << "] ";
	std::cout << "ERROR stopping acquisition ";
	std::cout << std::endl;
      }
    
    global_status.acq_running = false;

    std::chrono::time_point<std::chrono::system_clock> stop_time = std::chrono::system_clock::now();
    auto delta_time = std::chrono::duration_cast<std::chrono::duration<long int>>(stop_time - global_status.start_time);

    global_status.stop_time = stop_time;

    if (verbosity > 0)
      {
        char time_buffer[BUFFER_SIZE];
        time_string(time_buffer, BUFFER_SIZE, NULL);
        std::cout << '[' << time_buffer << "] ";
        std::cout << "Run time: ";
        std::cout << delta_time.count();
        std::cout << std::endl;
      }
}

void actions::generic::clear_memory(status &global_status)
{
  global_status.counts.clear();
  global_status.partial_counts.clear();
  global_status.waveforms_buffer.clear();
  // RP
  free(global_status.buff_trig);
}

void actions::generic::destroy_digitizer(status &global_status)
{
  // zpe
  std::cout << "\n\n DISTRUGGO IL DIGITIZZER!\n\n";
  if (rp_Release() != RP_OK) 
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR releasing device ";
      std::cout << std::endl;
    }
  

  global_status.digitizer_active = false;  
}

bool actions::generic::create_digitizer(status &global_status)
{
    if (global_status.verbosity > 0)
    {
        char time_buffer[BUFFER_SIZE];
        time_string(time_buffer, BUFFER_SIZE, NULL);
        std::cout << '[' << time_buffer << "] ";
        std::cout << "Opening device number: " << global_status.device_number << "; ";
        std::cout << std::endl;
    }

    //zpe
    std::cout << "\n\nINIZIALIZZO IL DIGITIZER\n\n";
    int result = rp_Init();

    if (result != RP_OK)
    {
        char szError[512];
        char time_buffer[BUFFER_SIZE];
        time_string(time_buffer, BUFFER_SIZE, NULL);
        std::cout << '[' << time_buffer << "] ";
        std::cout << "ERROR: Unable to open device: " << szError;
        std::cout << std::endl;
        return false;
    }
    global_status.digitizer_active = true;

    return true;
}

bool actions::generic::configure_digitizer(status &global_status)
{
  unsigned int verbosity = global_status.verbosity;

  global_status.enabled_channels.clear();
  
  //zpe
  std::cout << "\n\n CONFIGURO IL DIGITIZER\n\n";
    
  //json_t *config = global_status.config;
  json_t *config = json_object_get(global_status.config, "dgtzs");
  
  int channel_mask = json_number_value(json_object_get(config, "ChannelEnableMask"));
  global_status.channel_mask = channel_mask;
  if (channel_mask == 1 || channel_mask == 2) global_status.channels_number = 1;
  else  global_status.channels_number = 2;
  std::cout << "\n\n HO LETTO NEL CONFIG: channel_mask = " << channel_mask << std::endl;
  // manca gestione dell'errore

  // zpe
  int samples_number = json_number_value(json_object_get(config, "ScopeSamples"));
  global_status.samples_number = samples_number;
  std::cout << "\n\n HO LETTO NEL CONFIG: samples_number = " << samples_number << std::endl;
  // manca gestione dell'errore

  
  // if (ad2_handler == hdwfNone)
  if (global_status.digitizer_active == false)
  {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: Digitizer unavailable";
      std::cout << std::endl;  
      return false;
  }
  
  if (verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "Number of available channels: " << global_status.channels_number << "; ";
      std::cout << std::endl;
    }

  // Starting the trigger configuration
  json_t *json_trigger = json_object_get(config, "trigger");
  
  if (json_trigger != NULL )
    {
      // zpe
      int pretrigger = json_number_value(json_object_get(json_trigger, "pretrigger"));
      global_status.trigger_pretrigger = pretrigger;
      std::cout << "pretrigger = " << pretrigger << std::endl;
      // manca gestione dell'errore      size_t index;
 
      int triggersource = json_number_value(json_object_get(json_trigger, ""));
      global_status.trigger_source_channel = triggersource;
      std::cout << "trigger source = " << triggersource << std::endl;
      // manca gestione dell'errore      size_t index;

      float triggerlevel = json_number_value(json_object_get(json_trigger, ""));
      global_status.trigger_level = triggerlevel;
      std::cout << "trigger source = " << triggerlevel << std::endl;
      // manca gestione dell'errore      size_t index;

      int triggerslope = json_number_value(json_object_get(json_trigger, ""));
      global_status.trigger_slope = triggerslope;
      std::cout << "trigger slope = " << triggerslope << std::endl;
      // manca gestione dell'errore      size_t index;
    }
  
 
  /////////////
  // Starting the single channels configuration
  for (int ch=0; ch<2; ch++) {    
    // std::ostringstream oss;
    // oss << "ch" << ch;
    char chan[5]; sprintf(chan,"ch%d",ch);
    json_t *json_channel = json_object_get(config, chan);
    if (!json_channel) continue;
    
    const int id = ch;

    if (verbosity > 0)
      {
	char time_buffer[BUFFER_SIZE];
	time_string(time_buffer, BUFFER_SIZE, NULL);
	std::cout << '[' << time_buffer << "] ";
	std::cout << "Found channel: " << id << "; ";
	std::cout << std::endl;
      }

    const bool enabled = json_is_true(json_object_get(json_channel, "enable"));
    std::cout << "\n\n HO LETTO NEL CONFIG: enable = " << enabled << std::endl;
    // manca gestione dell'errore      size_t index;
    
    
    // const bool enabled = json_is_true(json_object_get(value, "enable"));
    
    if (verbosity > 0 && enabled)
      {
	char time_buffer[BUFFER_SIZE];
	time_string(time_buffer, BUFFER_SIZE, NULL);
	std::cout << '[' << time_buffer << "] ";
	std::cout << "Channel is enabled; ";
	std::cout << std::endl;
      }
    else if (verbosity > 0 && !enabled)
      {
	char time_buffer[BUFFER_SIZE];
	time_string(time_buffer, BUFFER_SIZE, NULL);
	std::cout << '[' << time_buffer << "] ";
	std::cout << "Channel is disabled; ";
	std::cout << std::endl;
      }
    
    if (enabled)
      {
	global_status.enabled_channels.push_back(id);
      }
    
  } // end channel config 
  



  // // Starting the single channels configuration
  // json_t *json_channels = json_object_get(config, "channels");
  
  // if (json_channels != NULL && json_is_array(json_channels))
  //   {
  //     size_t index;
  //     json_t *value;
      
  //     json_array_foreach(json_channels, index, value) {
  // 	json_t *json_id = json_object_get(value, "id");
	
  // 	if (json_id != NULL && json_is_integer(json_id)) {
  // 	  const int id = json_integer_value(json_id);
	  
  // 	  if (verbosity > 0)
  // 	    {
  // 	      char time_buffer[BUFFER_SIZE];
  // 	      time_string(time_buffer, BUFFER_SIZE, NULL);
  // 	      std::cout << '[' << time_buffer << "] ";
  // 	      std::cout << "Found channel: " << id << "; ";
  // 	      std::cout << std::endl;
  // 	    }
	  
  // 	  const bool enabled = json_is_true(json_object_get(value, "enable"));
	  
  // 	  if (verbosity > 0 && enabled)
  // 	    {
  // 	      char time_buffer[BUFFER_SIZE];
  // 	      time_string(time_buffer, BUFFER_SIZE, NULL);
  // 	      std::cout << '[' << time_buffer << "] ";
  // 	      std::cout << "Channel is enabled; ";
  // 	      std::cout << std::endl;
  // 	    }
  // 	  else if (verbosity > 0 && !enabled)
  // 	    {
  // 	      char time_buffer[BUFFER_SIZE];
  // 	      time_string(time_buffer, BUFFER_SIZE, NULL);
  // 	      std::cout << '[' << time_buffer << "] ";
  // 	      std::cout << "Channel is disabled; ";
  // 	      std::cout << std::endl;
  // 	    }
	  
  // 	  if (enabled)
  // 	    {
  // 	      global_status.enabled_channels.push_back(id);
  // 	    }
	  
  // 	}
  //     }
  //   } // end channel config alla Fontana
  
  rp_AcqReset();

  // Decimation 
  if (rp_AcqSetDecimation( (rp_acq_decimation_t)1 ) != RP_OK)
  {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR in AcqSetDecimation";
      std::cout << std::endl;  
      return false;
  }  

  // Trigger source
  global_status.tri_src = RP_TRIG_SRC_CHA_NE; // default value
  if (global_status.trigger_source_channel == 1 && global_status.trigger_slope == 1) global_status.tri_src = RP_TRIG_SRC_CHB_NE;
  else if (global_status.trigger_source_channel == 1 && global_status.trigger_slope == 0) global_status.tri_src = RP_TRIG_SRC_CHB_PE;
  else if (global_status.trigger_source_channel == 0 && global_status.trigger_slope == 0) global_status.tri_src = RP_TRIG_SRC_CHA_PE;
  if ( rp_AcqSetTriggerSrc(global_status.tri_src) != RP_OK)
  {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR in AcqSetTriggerSrc";
      std::cout << std::endl;  
      return false;
  }  
  
  // Trigger level
  rp_channel_t tricha = RP_CH_1; // default value
  if (global_status.trigger_source_channel == 1) tricha = RP_CH_2;
  if ( rp_AcqSetTriggerLevel(tricha, global_status.trigger_level) != RP_OK)
  {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR in AcqSetTriggerLevel";
      std::cout << std::endl;  
      return false;
  } 

  // Trigger delay - NOT CLEAR IF REALLY USEFUL...
  if ( rp_AcqSetTriggerDelay(global_status.trigger_delay) != RP_OK)
  {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR in AcqSetTriggerDelay";
      std::cout << std::endl;  
      return false;
  } 
  
  char time_buffer[BUFFER_SIZE];
  time_string(time_buffer, BUFFER_SIZE, NULL);
  std::cout << '[' << time_buffer << "] ";
  std::cout << "RP configuration completed successfully!";
  std::cout << std::endl;  
  
  return true;
}

bool actions::generic::allocate_memory(status &global_status)
{
  unsigned int verbosity = global_status.verbosity;
  
  global_status.counts.clear();
  global_status.counts.resize(global_status.channels_number, 0);
  global_status.partial_counts.clear();
  global_status.partial_counts.resize(global_status.channels_number, 0);
  
  if (verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "Memory allocation";
      std::cout << std::endl;
    }
  
  // Reserve the events_buffer in order to have a good starting size of its buffer
  global_status.waveforms_buffer.reserve(global_status.events_buffer_max_size \
					 +				\
					 global_status.events_buffer_max_size / 10);
  
  global_status.buff_trig = (int16_t*)malloc(global_status.samples_number * sizeof(int16_t) * 2);
  if (!global_status.buff_trig)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "Memory allocation";
      std::cout << std::endl;
      return false;
    }

  return true;
}

/******************************************************************************/
/* Specific actions                                                           */
/******************************************************************************/

state actions::start(status&)
{
  return states::CREATE_CONTEXT;
}

state actions::create_context(status &global_status)
{
  // Creates a ØMQ context
  void *context = zmq_ctx_new();
  if (!context)
    {
      // No errors are defined for this function
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: ZeroMQ Error on context creation";
      std::cout << std::endl;
      
      return states::COMMUNICATION_ERROR;
    }
  
  global_status.context = context;
  
  std::this_thread::sleep_for(std::chrono::milliseconds(defaults_abcd_zmq_delay));
  
  return states::CREATE_SOCKETS;
}

state actions::create_sockets(status &global_status)
{
  void *context = global_status.context;
  
  // Creates the status socket
  void *status_socket = zmq_socket(context, ZMQ_PUB);
  if (!status_socket)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: ZeroMQ Error on status socket creation: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
      
      return states::COMMUNICATION_ERROR;
    }
  
  // Creates the data socket
  void *data_socket = zmq_socket(context, ZMQ_PUB);
  if (!data_socket)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: ZeroMQ Error on data socket creation: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
      
      return states::COMMUNICATION_ERROR;
    }
  
  // Creates the commands socket
  void *commands_socket = zmq_socket(context, ZMQ_PULL);
  if (!commands_socket)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: ZeroMQ Error on commands socket creation: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
      
      return states::COMMUNICATION_ERROR;
    }
  
  global_status.status_socket = status_socket;
  global_status.data_socket = data_socket;
  global_status.commands_socket = commands_socket;
  
  std::this_thread::sleep_for(std::chrono::milliseconds(defaults_abcd_zmq_delay));
  
  return states::BIND_SOCKETS;
}

state actions::bind_sockets(status &global_status)
{
  std::string status_address = global_status.status_address;
  std::string data_address = global_status.data_address;
  std::string commands_address = global_status.commands_address;
  
  // Binds the status socket to its address
  const int s = zmq_bind(global_status.status_socket, status_address.c_str());
  if (s != 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: ZeroMQ Error on status socket binding: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
      
      return states::COMMUNICATION_ERROR;
    }
  
  // Binds the data socket to its address
  const int d = zmq_bind(global_status.data_socket, data_address.c_str());
  if (d != 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: ZeroMQ Error on data socket binding: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
      
      return states::COMMUNICATION_ERROR;
    }
  
  // Binds the socket to its address
  const int c = zmq_bind(global_status.commands_socket, commands_address.c_str());
  if (c != 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: ZeroMQ Error on commands socket binding: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
      
      return states::COMMUNICATION_ERROR;
    }
  
  std::this_thread::sleep_for(std::chrono::milliseconds(defaults_abcd_zmq_delay));
  
  return states::READ_CONFIG;
}

state actions::read_config(status &global_status)
{
  const std::string config_file_name = global_status.config_file;
  
  if (global_status.verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "Reading config file: " << config_file_name << " ";
      std::cout << std::endl;
    }
  
  json_error_t error;
  
  json_t *new_config = json_load_file(config_file_name.c_str(), 0, &error);
  
  if (!new_config)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: Parse error while reading config file: ";
      std::cout << error.text;
      std::cout << " (source: ";
      std::cout << error.source;
      std::cout << ", line: ";
      std::cout << error.line;
      std::cout << ", column: ";
      std::cout << error.column;
      std::cout << ", position: ";
      std::cout << error.position;
      std::cout << "); ";
      std::cout << std::endl;
      
      return states::PARSE_ERROR;
    }
  else
    {
      global_status.config = new_config;
      
      return states::CREATE_DIGITIZER;
    }
}

state actions::create_digitizer(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("event"));
  json_object_set_new_nocheck(json_event_message, "event", json_string("Digitizer initialization"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  const bool success = actions::generic::create_digitizer(global_status);
  
  if (success)
    {
      return states::CONFIGURE_DIGITIZER;
    }
  else
    {
      return states::CONFIGURE_ERROR;
    }
}

state actions::recreate_digitizer(status &global_status)
{
  const bool success = actions::generic::create_digitizer(global_status);
  
  if (success)
    {
      return states::CONFIGURE_DIGITIZER;
    }
  else
    {
      return states::CONFIGURE_ERROR;
    }
}


state actions::configure_digitizer(status &global_status)
{
  const bool success = actions::generic::configure_digitizer(global_status);
  
  if (success)
    {
      return states::ALLOCATE_MEMORY;
    }
  else
    {
      return states::CONFIGURE_ERROR;
    }
}

state actions::reconfigure_destroy_digitizer(status &global_status)
{
  actions::generic::destroy_digitizer(global_status);
  
  return states::RECREATE_DIGITIZER;
}

state actions::receive_commands(status &global_status)
{
  void *commands_socket = global_status.commands_socket;
  
  char *buffer;
  size_t size;
  
  const int result = receive_byte_message(commands_socket, nullptr, (void**)(&buffer), &size, 0, global_status.verbosity);
  
  if (global_status.verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "result: " << result << "; ";
      std::cout << "size: " << size << "; ";
      std::cout << std::endl;
    }
  
  if (size > 0 && result == EXIT_SUCCESS)
    {
      if (global_status.verbosity > 0)
        {
	  char time_buffer[BUFFER_SIZE];
	  time_string(time_buffer, BUFFER_SIZE, NULL);
	  std::cout << '[' << time_buffer << "] ";
	  std::cout << "Message buffer: ";
	  std::cout << (char*)buffer;
	  std::cout << "; ";
	  std::cout << std::endl;
        }
      
      std::string error_string;
      
      json_error_t error;
      json_t *json_message = json_loadb(buffer, size, 0, &error);
      
      free(buffer);
      
      if (!json_message)
        {
	  char time_buffer[BUFFER_SIZE];
	  time_string(time_buffer, BUFFER_SIZE, NULL);
	  std::cout << '[' << time_buffer << "] ";
	  std::cout << "ERROR: ";
	  std::cout << error.text;
	  std::cout << " (source: ";
	  std::cout << error.source;
	  std::cout << ", line: ";
	  std::cout << error.line;
	  std::cout << ", column: ";
	  std::cout << error.column;
	  std::cout << ", position: ";
	  std::cout << error.position;
	  std::cout << "); ";
	  std::cout << std::endl;
        }
      else
        {
	  const size_t command_ID = json_integer_value(json_object_get(json_message, "msg_ID"));
	  const std::string command = json_string_value(json_object_get(json_message, "command"));
	  json_t *json_arguments = json_object_get(json_message, "arguments");
	  
	  if (global_status.verbosity > 0)
            {
	      char time_buffer[BUFFER_SIZE];
	      time_string(time_buffer, BUFFER_SIZE, NULL);
	      std::cout << '[' << time_buffer << "] ";
	      std::cout << "Command ID: " << command_ID << "; ";
	      std::cout << std::endl;
            }
	  
	  if (command == std::string("start"))
            {
	      char time_buffer[BUFFER_SIZE];
	      time_string(time_buffer, BUFFER_SIZE, NULL);
	      std::cout << '[' << time_buffer << "] ";
	      std::cout << "################################################################### Start!!! ###";
	      std::cout << std::endl;
	      
	      return states::START_ACQUISITION;
            }
	  else if (command == std::string("reconfigure") && json_arguments)
            {
	      json_t *new_config = json_object_get(json_arguments, "config");
	      
	      // Remember to clean up
	      json_decref(global_status.config);
	      
	      // This is a borrowed reference, so we shall increment the reference count
	      global_status.config = new_config;
	      json_incref(global_status.config);
	      
	      json_t *json_event_message = json_object();
	      
	      json_object_set_new_nocheck(json_event_message, "type", json_string("event"));
	      json_object_set_new_nocheck(json_event_message, "event", json_string("Digitizer reconfiguration"));
	      
	      actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
	      
	      json_decref(json_event_message);
	      
	      return states::CONFIGURE_DIGITIZER;
            }
	  else if (command == std::string("off"))
            {
	      return states::CLEAR_MEMORY;
            }
	  else if (command == std::string("quit"))
            {
	      return states::CLEAR_MEMORY;
            }
	  
	  json_decref(json_message);
        }
    }
  
  const std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
  if (now - global_status.last_publication > std::chrono::seconds(defaults_abcd_publish_timeout))
    {
      return states::PUBLISH_STATUS;
    }
  
  return states::RECEIVE_COMMANDS;
}

state actions::publish_status(status &global_status)
{
  const bool poll_success = true;

  //zpe
  std::cout << "\n\nPUBBLICO LO STATUS\n\n";

  if (global_status.verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << std::endl;
    }

    json_t *status_message = json_object();

    json_object_set_new_nocheck(status_message, "config", json_deep_copy(global_status.config));
    
    json_t *acquisition = json_object();
    json_object_set_new_nocheck(acquisition, "running", json_false());
    json_object_set_new_nocheck(status_message, "acquisition", acquisition);
    
    json_t *digitizer = json_object();
    
    //if (global_status.ad2_handler == hdwfNone || poll_success == false)
    if (global_status.digitizer_active == false || poll_success == false)
      {
        json_object_set_new_nocheck(digitizer, "valid_pointer", json_false());
        json_object_set_new_nocheck(digitizer, "active", json_false());
      }
    else
      {
        json_object_set_new_nocheck(digitizer, "valid_pointer", json_true());
        json_object_set_new_nocheck(digitizer, "active", json_true());
      }
    
    json_object_set_new_nocheck(status_message, "digitizer", digitizer);
    
    actions::generic::publish_message(global_status, defaults_abcd_status_topic, status_message);
    
    json_decref(status_message);
    
    
    //if (global_status.ad2_handler != hdwfNone && poll_success == true)
    if ( global_status.digitizer_active && poll_success == true)
    {
        return states::RECEIVE_COMMANDS;
    }
    else
    {
        return states::DIGITIZER_ERROR;
    }
}

state actions::allocate_memory(status &global_status)
{
    const bool success = actions::generic::allocate_memory(global_status);
    
    if (success)
      {
        return states::PUBLISH_STATUS;
      }
    else
      {
        return states::DIGITIZER_ERROR;
      }
}

state actions::restart_allocate_memory(status &global_status)
{
  const bool success = actions::generic::allocate_memory(global_status);
  
  if (success)
    {
      return states::START_ACQUISITION;
    }
  else
    {
      return states::RESTART_CONFIGURE_ERROR;
    }
}

state actions::stop(status&)
{
  return states::STOP;
}

state actions::start_acquisition(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("event"));
  json_object_set_new_nocheck(json_event_message, "event", json_string("Start acquisition"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  std::chrono::time_point<std::chrono::system_clock> start_time = std::chrono::system_clock::now();
  
  //if (global_status.ad2_handler == hdwfNone)
  if (global_status.digitizer_active == false)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: Digitizer pointer unavailable";
      std::cout << std::endl;      
      return states::ACQUISITION_ERROR;
    }
  
  if (global_status.verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "Clearing counters; ";
      std::cout << std::endl;
    }
  
  const int channels_number = global_status.channels_number;
  
  global_status.counts.clear();
  global_status.counts.resize(channels_number, 0);
  global_status.partial_counts.clear();
  global_status.partial_counts.resize(channels_number, 0);
  
  // Start acquisition
  //zpe
  std::cout << "\n\nFACCIO PARTIRE l'ACQUISIZIONE...\n\n";
  
  if (rp_AcqSetArmKeep(1) != RP_OK) {
    char time_buffer[BUFFER_SIZE];
    time_string(time_buffer, BUFFER_SIZE, NULL);
    std::cout << '[' << time_buffer << "] ";
    std::cout << "ERROR in AcqSetArmKeep ";
    std::cout << std::endl;
    // da capire cosa fargli fare qui...
  }

  global_status.acq_running = true;

  global_status.start_time = start_time;
  
  return states::ACQUISITION_RECEIVE_COMMANDS;
}

state actions::acquisition_receive_commands(status &global_status)
{
  void *commands_socket = global_status.commands_socket;
  
  char *buffer;
  size_t size;
  
  const int result = receive_byte_message(commands_socket, nullptr, (void**)(&buffer), &size, 0, global_status.verbosity);
  
  if (global_status.verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "result: " << result << "; ";
      std::cout << "size: " << size << "; ";
      std::cout << std::endl;
    }
  
  if (size > 0 && result == EXIT_SUCCESS)
    {
      if (global_status.verbosity > 0)
        {
	  char time_buffer[BUFFER_SIZE];
	  time_string(time_buffer, BUFFER_SIZE, NULL);
	  std::cout << '[' << time_buffer << "] ";
	  std::cout << "Message buffer: ";
	  std::cout << (char*)buffer;
	  std::cout << "; ";
	  std::cout << std::endl;
        }
      
      std::string error_string;
      
      json_error_t error;
      json_t *json_message = json_loadb(buffer, size, 0, &error);
      
      free(buffer);
      
      if (!json_message)
        {
	  char time_buffer[BUFFER_SIZE];
	  time_string(time_buffer, BUFFER_SIZE, NULL);
	  std::cout << '[' << time_buffer << "] ";
	  std::cout << "ERROR: ";
	  std::cout << error.text;
	  std::cout << " (source: ";
	  std::cout << error.source;
	  std::cout << ", line: ";
	  std::cout << error.line;
	  std::cout << ", column: ";
	  std::cout << error.column;
	  std::cout << ", position: ";
	  std::cout << error.position;
	  std::cout << "); ";
	  std::cout << std::endl;
        }
      else
        {
	  const size_t command_ID = json_integer_value(json_object_get(json_message, "msg_ID"));
	  const std::string command = json_string_value(json_object_get(json_message, "command"));
	  
	  if (global_status.verbosity > 0)
            {
	      char time_buffer[BUFFER_SIZE];
	      time_string(time_buffer, BUFFER_SIZE, NULL);
	      std::cout << '[' << time_buffer << "] ";
	      std::cout << "Command ID: " << command_ID << "; ";
	      std::cout << std::endl;
            }
	  
	  if (command == std::string("stop"))
            {
	      char time_buffer[BUFFER_SIZE];
	      time_string(time_buffer, BUFFER_SIZE, NULL);
	      std::cout << '[' << time_buffer << "] ";
	      std::cout << "#################################################################### Stop!!! ###";
	      std::cout << std::endl;
	      
	      return states::STOP_PUBLISH_EVENTS;
            }
	  
	  json_decref(json_message);
        }
    }
  
  return states::ADD_TO_BUFFER;
}

state actions::add_to_buffer(status &global_status)
{
  const unsigned int verbosity = global_status.verbosity;

  //zpe
  //std::cout << "\n\nQUI LEGGO UN EVENTO E LO METTO NEL BUFFER...\n\n";
   
  const std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
  
  // zpe
  if (global_status.digitizer_active == false)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: Digitizer unavailable";
      std::cout << std::endl;
      return states::ACQUISITION_ERROR;
    }
  
  // fa ripartire ogni volta 
  if (rp_AcqStart() != RP_OK)
  {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR in AcqStart";
      std::cout << std::endl;  
      // da capire cosa deve fare ora...
      return states::ACQUISITION_ERROR;
  }  

  // deve anche riarmare il trigger  
  if ( rp_AcqSetTriggerSrc(global_status.tri_src) != RP_OK)
  {
    char time_buffer[BUFFER_SIZE];
    time_string(time_buffer, BUFFER_SIZE, NULL);
    std::cout << '[' << time_buffer << "] ";
    std::cout << "ERROR in AcqSetTriggerSrc inside acquisition loop";
    std::cout << std::endl;  
    // da capire cosa deve fare ora
    return states::ACQUISITION_ERROR;
  }  
  rp_acq_trig_state_t state = RP_TRIG_STATE_TRIGGERED;
  bool poll_success = false;

  // QUI CECCKA LO STATUS DI ACQUISIZIONE  
  int check_counter=0;
  while(1){
    if (rp_AcqGetTriggerState(&state) != RP_OK)
      {
	char time_buffer[BUFFER_SIZE];
	time_string(time_buffer, BUFFER_SIZE, NULL);
	std::cout << '[' << time_buffer << "] ";
	std::cout << "ERROR in AcqGetTriggerState inside acquisition loop";
	std::cout << std::endl;  
	// da capire cosa deve fare ora
	return states::ACQUISITION_ERROR;
      }  
    check_counter++;
    if(state == RP_TRIG_STATE_TRIGGERED){
      poll_success = true;
      break;
    }
    if (check_counter>global_status.maxcounter) {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "Counter oversize! skipping event loop";
      std::cout << std::endl; 
      poll_success = false;
      break;
    }
  }
  
  // if (verbosity > 0)
  //   {
  //     const std::chrono::time_point<std::chrono::system_clock> after_poll = std::chrono::system_clock::now();
  //     const auto poll_duration = std::chrono::duration_cast<nanoseconds>(after_poll - now).count();
      
  //     char time_buffer[BUFFER_SIZE];
  //     time_string(time_buffer, BUFFER_SIZE, NULL);
  //     std::cout << '[' << time_buffer << "] ";
  //     std::cout << "Poll duration: " << poll_duration * 1e-6 << " ms; ";
  //     std::cout << std::endl;
  //   }
  
  // if (!poll_success)
  //   {
  //     char time_buffer[BUFFER_SIZE];
  //     time_string(time_buffer, BUFFER_SIZE, NULL);
  //     std::cout << '[' << time_buffer << "] ";
  //     std::cout << "ERROR: Digitizer error";
  //     std::cout << std::endl;
  //     return states::ACQUISITION_ERROR;
  //   }
  
  // zpe
  // QUI HA RICEVUTO L'EVENTO E SCARICA IL BUFFER
  //    if (ad2_status == DwfStateDone)
  if (poll_success)
    {
      if (verbosity > 0)
	{
	  char time_buffer[BUFFER_SIZE];
	  time_string(time_buffer, BUFFER_SIZE, NULL);
	  std::cout << '[' << time_buffer << "] ";
	  std::cout << "Downloading samples; ";
	  std::cout << std::endl;
	}
      
      uint32_t trigpos=0;
      if (rp_AcqGetWritePointerAtTrig(&trigpos) != RP_OK)  // recover trigger position
	{
	  char time_buffer[BUFFER_SIZE];
	  time_string(time_buffer, BUFFER_SIZE, NULL);
	  std::cout << '[' << time_buffer << "] ";
	  std::cout << "ERROR in  AcqGetWritePointerAtTrig inside acquisition loop";
	  std::cout << std::endl;
	  return states::ACQUISITION_ERROR;
	  //da capire cosa fare ora...
	}
      
      //std::vector<double> double_samples(global_status.ad2_buffer_size);
      std::vector<int16_t> int_samples(global_status.samples_number);

      // We have events...
      // zpe
      // LOOP SUI CANALI, DA RIVEDERE...
      for (auto &channel: global_status.enabled_channels)
	{
	  const auto Delta = std::chrono::duration_cast<nanoseconds>(now - global_status.start_time);
	  const uint64_t timestamp = Delta.count();
	  
	  if (verbosity > 0)
	    {
	      char time_buffer[BUFFER_SIZE];
	      time_string(time_buffer, BUFFER_SIZE, NULL);
	      std::cout << '[' << time_buffer << "] ";
	      std::cout << "Downloading samples from channel: " << channel << "; ";
	      std::cout << std::endl;
	    }
	  
	  const std::chrono::time_point<std::chrono::system_clock> before_data_reading = std::chrono::system_clock::now();
	  
	  // zpepeee
	  bool download_success = true;

	  if (trigpos > (uint32_t)global_status.trigger_pretrigger && trigpos < (uint32_t)(POSMAX-global_status.samples_number) )
	    {
	      
	      // char time_buffer[BUFFER_SIZE];
	      // time_string(time_buffer, BUFFER_SIZE, NULL);
	      // std::cout << '[' << time_buffer << "] ";
	      // std::cout << "sto per leggere i dati...";
	      // std::cout << std::endl;	

	      if (rp_AcqGetDataRaw((rp_channel_t)channel, (uint32_t)(trigpos-global_status.trigger_pretrigger), &global_status.samples_number, int_samples.data()) != RP_OK)      
	      //if (rp_AcqGetDataRaw((rp_channel_t)channel, (uint32_t)(trigpos-global_status.trigger_pretrigger), &global_status.samples_number, global_status.buff_trig) != RP_OK)      
		{
		  char time_buffer[BUFFER_SIZE];
		  time_string(time_buffer, BUFFER_SIZE, NULL);
		  std::cout << '[' << time_buffer << "] ";
		  std::cout << "ERROR in  AcqGetDataRaw inside acquisition loop";
		  std::cout << std::endl;
		  return states::ACQUISITION_ERROR;
		  //da capire cosa fare ora...
		}	  
	      download_success = true;

	      // time_string(time_buffer, BUFFER_SIZE, NULL);
	      // std::cout << '[' << time_buffer << "] ";
	      // std::cout << "ho letto i dati...";
	      // std::cout << std::endl;

	    }
	  
	  // QUI DECODIFICO IL BUFFER DI EVENTI
	  // GENERA UN EVENTO FITTIZIO A RAMPA	    
	  //for (int jj=0; jj<global_status.trigger_pretrigger; jj++) int_samples[jj] = std::rand()/200000000-5; // noise 5 ch
	  //for (int jj=global_status.trigger_pretrigger; jj<global_status.samples_number/2; jj++) int_samples[jj]=-jj;
	  //for (int jj=global_status.samples_number/2; jj<global_status.samples_number; jj++) int_samples[jj] = std::rand()/200000000-5;
	  //std::cout << "... ho generato un evento fasullo di " << global_status.samples_number << " points " << std::endl;
	  
	  if (verbosity > 0)
	    {
	      const auto data_reading_duration = std::chrono::duration_cast<nanoseconds>(std::chrono::system_clock::now() - before_data_reading).count();
	      
	      char time_buffer[BUFFER_SIZE];
	      time_string(time_buffer, BUFFER_SIZE, NULL);
	      std::cout << '[' << time_buffer << "] ";
	      std::cout << "Data reading duration: " << data_reading_duration * 1e-6 << " ms; ";
	      std::cout << std::endl;
	    }
	  
	  if (!download_success)
	    {
	      char time_buffer[BUFFER_SIZE];
	      time_string(time_buffer, BUFFER_SIZE, NULL);
	      std::cout << '[' << time_buffer << "] ";
	      std::cout << "ERROR: Unable to read data";
	      std::cout << std::endl;
	      
	      return states::ACQUISITION_ERROR;
	    }
	  
	  
	  global_status.counts[channel] += 1;
	  global_status.partial_counts[channel] += 1;
	  
	  global_status.waveforms_buffer.emplace_back(timestamp,
						      channel,
						      global_status.samples_number);
	  
	  // zpe - DA VERIFICARE
	  // Trasforming the signed int to an unsigned int for compatibility with the rest of ABCD
	  for (int j = 0; j < (int)global_status.samples_number; j++)
	    {
	      global_status.waveforms_buffer.back().samples[j] = int_samples[j] + (1 << 13);
	    }
	}
      
      if (verbosity > 0)
	{
	  char time_buffer[BUFFER_SIZE];
	  time_string(time_buffer, BUFFER_SIZE, NULL);
	  std::cout << '[' << time_buffer << "] ";
	  std::cout << "Waveforms buffer size: " << global_status.waveforms_buffer.size() << "; ";
	  std::cout << std::endl;
	}
      
      if (global_status.waveforms_buffer.size() >= global_status.events_buffer_max_size)
	{
	  return states::PUBLISH_EVENTS;
	}
    }
  
  if (now - global_status.last_publication > std::chrono::seconds(defaults_abcd_publish_timeout))
    {
      return states::PUBLISH_EVENTS;
    }
  
  return states::ADD_TO_BUFFER;
}

state actions::publish_events(status &global_status)
{
  actions::generic::publish_events(global_status);
  
  const std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
  if (now - global_status.last_publication > std::chrono::seconds(defaults_abcd_publish_timeout))
    {
      return states::ACQUISITION_PUBLISH_STATUS;
    }
  
  return states::ADD_TO_BUFFER;
}

state actions::stop_publish_events(status &global_status)
{
  actions::generic::publish_events(global_status);
  
  return states::STOP_ACQUISITION;
}

state actions::acquisition_publish_status(status &global_status)
{

  //zpe
  std::cout << "\n\nPUBBLICO L'ACQUSITION STATUS\n\n";

  //DwfState ad2_status;
  //const bool poll_success = FDwfAnalogInStatus(global_status.ad2_handler, false, &ad2_status);
  const bool poll_success = true;
  
  if (global_status.verbosity > 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << std::endl;
    }
  
  json_t *status_message = json_object();
  
  json_object_set_new_nocheck(status_message, "config", json_deep_copy(global_status.config));
  
  json_t *acquisition = json_object();
  json_t *digitizer = json_object();
  
  // if (global_status.ad2_handler == hdwfNone || !poll_success)
  if (global_status.digitizer_active == false || !poll_success)
    {
      json_object_set_new_nocheck(digitizer, "valid_pointer", json_false());
      json_object_set_new_nocheck(digitizer, "active", json_false());
    }
  else
    {
      json_object_set_new_nocheck(digitizer, "valid_pointer", json_true());
      json_object_set_new_nocheck(digitizer, "active", json_true());
      
      json_object_set_new_nocheck(acquisition, "running", json_true());
      
      const auto now = std::chrono::system_clock::now();
      
      const auto run_delta_time = std::chrono::duration_cast<std::chrono::duration<long int>>(now - global_status.start_time);
      const long int runtime = run_delta_time.count();
      
      json_object_set_new_nocheck(acquisition, "runtime", json_integer(runtime));
      
      const auto pub_delta_time = std::chrono::duration_cast<std::chrono::duration<long int>>(now - global_status.last_publication);
      const double pubtime = static_cast<double>(pub_delta_time.count());
      
      json_t *rates = json_array();
      json_t *ICR_rates = json_array();
      
      json_t *counts = json_array();
      json_t *ICR_counts = json_array();
      
      if (pubtime > 0)
        {
	  for (unsigned int i = 0; i < global_status.partial_counts.size(); i++)
            {
	      const double rate = global_status.partial_counts[i] / pubtime;
	      json_array_append_new(rates, json_real(rate));
	      json_array_append_new(ICR_rates, json_real(rate));
            }
        }
      else
        {
	  for (unsigned int i = 0; i < global_status.partial_counts.size(); i++)
            {
	      json_array_append_new(rates, json_integer(0));
	      json_array_append_new(ICR_rates, json_integer(0));
            }
        }
      
      for (unsigned int i = 0; i < global_status.counts.size(); i++)
        {
	  const unsigned int channel_counts = global_status.counts[i];
	  json_array_append_new(counts, json_integer(channel_counts));
	  json_array_append_new(ICR_counts, json_integer(channel_counts));
        }
      
      json_object_set_new_nocheck(acquisition, "rates", rates);
      json_object_set_new_nocheck(acquisition, "ICR_rates", ICR_rates);
      
      json_object_set_new_nocheck(acquisition, "counts", counts);
      json_object_set_new_nocheck(acquisition, "ICR_counts", ICR_counts);
    }
  
  json_object_set_new_nocheck(status_message, "acquisition", acquisition);
  json_object_set_new_nocheck(status_message, "digitizer", digitizer);
  
  actions::generic::publish_message(global_status, defaults_abcd_status_topic, status_message);
  
  json_decref(status_message);
  
  // Clear event partial counts
  for (unsigned int i = 0; i < global_status.partial_counts.size(); i++)
    {
      global_status.partial_counts[i] = 0;
    }
  
  //if (global_status.ad2_handler != hdwfNone && poll_success == true)
  if (global_status.digitizer_active == true || poll_success == true)
    {
      return states::ACQUISITION_RECEIVE_COMMANDS;
    }
  else
    {
      return states::ACQUISITION_ERROR;
    }
}

state actions::stop_acquisition(status &global_status)
{
  actions::generic::stop_acquisition(global_status);
  
  auto delta_time = std::chrono::duration_cast<std::chrono::duration<long int>>(global_status.stop_time - global_status.start_time);
  const std::string event_message = "Stop acquisition (duration: " + std::to_string(delta_time.count()) + " s)";
  
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("event"));
  json_object_set_new_nocheck(json_event_message, "event", json_string(event_message.c_str()));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::RECEIVE_COMMANDS;
}

state actions::clear_memory(status &global_status)
{
  //if (global_status.ad2_handler != hdwfNone)
  if (global_status.digitizer_active == true)
    {
      actions::generic::clear_memory(global_status);
    }
  else
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: Digitizer pointer unavailable";
      std::cout << std::endl;
    }
  
  return states::DESTROY_DIGITIZER;
}

state actions::reconfigure_clear_memory(status &global_status)
{
  //if (global_status.ad2_handler != hdwfNone)
  if (global_status.digitizer_active == true)
    {
      actions::generic::clear_memory(global_status);
    }
  else
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: Digitizer pointer unavailable";
      std::cout << std::endl;
      
      return states::CONFIGURE_ERROR;
    }
  
  return states::RECONFIGURE_DESTROY_DIGITIZER;
}



state actions::destroy_digitizer(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("event"));
  json_object_set_new_nocheck(json_event_message, "event", json_string("Digitizer deactivation"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  actions::generic::destroy_digitizer(global_status);
  
  return states::CLOSE_SOCKETS;
}

state actions::restart_publish_events(status &global_status)
{
  actions::generic::publish_events(global_status);
  
  return states::RESTART_STOP_ACQUISITION;
}

state actions::restart_stop_acquisition(status &global_status)
{
  actions::generic::stop_acquisition(global_status);
  
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("event"));
  json_object_set_new_nocheck(json_event_message, "event", json_string("Restart stop acquisition"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::RESTART_CLEAR_MEMORY;
}

state actions::restart_clear_memory(status &global_status)
{
  //if (global_status.ad2_handler != hdwfNone)
  if (global_status.digitizer_active == true)
    {
      actions::generic::clear_memory(global_status);
    }
  else
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ERROR: Digitizer pointer unavailable";
      std::cout << std::endl;
    }
  
  return states::RESTART_DESTROY_DIGITIZER;
}


state actions::restart_destroy_digitizer(status &global_status)
{
  actions::generic::destroy_digitizer(global_status);
  
  return states::RESTART_CREATE_DIGITIZER;
}

state actions::restart_create_digitizer(status &global_status)
{
  const bool success = actions::generic::create_digitizer(global_status);
  
  if (success)
    {
      return states::RESTART_CONFIGURE_DIGITIZER;
    }
  else
    {
      return states::RESTART_CONFIGURE_ERROR;
    }
}

state actions::restart_configure_digitizer(status &global_status)
{
  const bool success = actions::generic::configure_digitizer(global_status);
  
  if (success)
    {
      return states::RESTART_ALLOCATE_MEMORY;
    }
  else
    {
      return states::RESTART_CONFIGURE_ERROR;
    }
}

state actions::close_sockets(status &global_status)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(defaults_abcd_zmq_delay));
  
  void *status_socket = global_status.status_socket;
  void *data_socket = global_status.data_socket;
  void *commands_socket = global_status.commands_socket;
  
  const int s = zmq_close(status_socket);
  if (s != 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ZeroMQ Error on status socket close: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
    }
  
  const int d = zmq_close(data_socket);
  if (d != 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ZeroMQ Error on data socket close: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
    }
  
  const int c = zmq_close(commands_socket);
  if (c != 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ZeroMQ Error on commands socket close: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
    }
  
  return states::DESTROY_CONTEXT;
}

state actions::destroy_context(status &global_status)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(defaults_abcd_zmq_delay));
  
  void *context = global_status.context;
  
  const int c = zmq_ctx_destroy(context);
  if (c != 0)
    {
      char time_buffer[BUFFER_SIZE];
      time_string(time_buffer, BUFFER_SIZE, NULL);
      std::cout << '[' << time_buffer << "] ";
      std::cout << "ZeroMQ Error on context destroy: ";
      std::cout << zmq_strerror(errno);
      std::cout << std::endl;
    }
  
  return states::STOP;
}

state actions::communication_error(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("error"));
  json_object_set_new_nocheck(json_event_message, "error", json_string("Communication error"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::CLOSE_SOCKETS;
}

state actions::parse_error(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("error"));
  json_object_set_new_nocheck(json_event_message, "error", json_string("Config parse error"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::CLOSE_SOCKETS;
}

state actions::configure_error(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("error"));
  json_object_set_new_nocheck(json_event_message, "error", json_string("Configure error"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::RECONFIGURE_DESTROY_DIGITIZER;
}

state actions::digitizer_error(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("error"));
  json_object_set_new_nocheck(json_event_message, "error", json_string("Digitizer error"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::RECONFIGURE_CLEAR_MEMORY;
}

state actions::acquisition_error(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("error"));
  json_object_set_new_nocheck(json_event_message, "error", json_string("Acquisition error"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::RESTART_PUBLISH_EVENTS;
}

state actions::restart_configure_error(status &global_status)
{
  json_t *json_event_message = json_object();
  
  json_object_set_new_nocheck(json_event_message, "type", json_string("error"));
  json_object_set_new_nocheck(json_event_message, "error", json_string("Restart configure error"));
  
  actions::generic::publish_message(global_status, defaults_abcd_events_topic, json_event_message);
  
  json_decref(json_event_message);
  
  return states::RESTART_DESTROY_DIGITIZER;
}
