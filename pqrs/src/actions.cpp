/*
 * (C) Copyright 2016 Cristiano Lino Fontana
 *
 * This file is part of ABCD.
 *
 * ABCD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ABCD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ABCD.  If not, see <http://www.gnu.org/licenses/>.
 */

// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <cstring>
#include <chrono>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <zmq.h>
#include <json/json.h>

#include "utilities_functions.hpp"
#include "socket_functions.hpp"
#include "typedefs.hpp"
#include "states.hpp"
#include "events.hpp"
#include "actions.hpp"

/******************************************************************************/
/* Generic actions                                                            */
/******************************************************************************/

void actions::generic::publish_status(status &global_status,
                                      std::string topic,
                                      Json::Value status_message)
{
    std::chrono::time_point<std::chrono::system_clock> last_publication = std::chrono::system_clock::now();
    global_status.last_publication = last_publication;

    void *status_socket = global_status.status_socket;
    const unsigned long int status_msg_ID = global_status.status_msg_ID;

    status_message["module"] = "pqrs";
    status_message["timestamp"] = utilities_functions::time_string();
    status_message["msg_ID"] = Json::Value::UInt64(status_msg_ID);

    socket_functions::send_json_message(status_socket, topic, status_message, 1);

    global_status.status_msg_ID += 1;
}

/******************************************************************************/
/* Specific actions                                                           */
/******************************************************************************/

state actions::start(status&)
{
    return states::CREATE_CONTEXT;
}

state actions::create_context(status &global_status)
{
    // Creates a ØMQ context
    void *context = zmq_ctx_new();
    if (!context)
    {
        // No errors are defined for this function
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on context creation";
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    global_status.context = context;

    return states::CREATE_SOCKETS;
}

state actions::create_sockets(status &global_status)
{
    void *context = global_status.context;

    // Creates the status socket
    void *status_socket = zmq_socket(context, ZMQ_PUB);
    if (!status_socket)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on status socket creation: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Creates the data socket
    void *data_socket = zmq_socket(context, ZMQ_PUB);
    if (!data_socket)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on data socket creation: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Creates the commands socket
    void *commands_socket = zmq_socket(context, ZMQ_PULL);
    if (!commands_socket)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on commands socket creation: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Creates the abcd data socket
    void *abcd_data_socket = zmq_socket(context, ZMQ_SUB);
    if (!abcd_data_socket)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on abcd data socket creation: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    global_status.status_socket = status_socket;
    global_status.data_socket = data_socket;
    global_status.commands_socket = commands_socket;
    global_status.abcd_data_socket = abcd_data_socket;

    return states::BIND_SOCKETS;
}

state actions::bind_sockets(status &global_status)
{
    std::string status_address = global_status.status_address;
    std::string data_address = global_status.data_address;
    std::string commands_address = global_status.commands_address;
    std::string abcd_data_address = global_status.abcd_data_address;

    // Binds the status socket to its address
    const int s = zmq_bind(global_status.status_socket, status_address.c_str());
    if (s != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on status socket binding: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Binds the status socket to its address
    const int d = zmq_bind(global_status.data_socket, data_address.c_str());
    if (d != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on status socket binding: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Binds the socket to its address
    const int c = zmq_bind(global_status.commands_socket, commands_address.c_str());
    if (c != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on commands socket binding: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Connects to the abcd data socket to its address
    const int a = zmq_connect(global_status.abcd_data_socket, abcd_data_address.c_str());
    if (a != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ERROR: ZeroMQ Error on abcd data socket connection: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;

        return states::COMMUNICATION_ERROR;
    }

    // Subscribe to data topic
    const std::string data_topic("data_abcd_events");
    zmq_setsockopt(global_status.abcd_data_socket, ZMQ_SUBSCRIBE, data_topic.c_str(), data_topic.size());

    return states::PUBLISH_STATUS;
}

state actions::publish_status(status &global_status)
{
    Json::Value active_channels;
    Json::Value qlong_configs;
    Json::Value PSD_configs;

    for (const unsigned int &channel: global_status.active_channels)
    {
        active_channels.append(channel);

        const auto qlong_histo = global_status.histos_qlong[channel];
        const Json::Value qlong_config = qlong_histo.config;

        qlong_configs[std::to_string(channel)] = qlong_config;

        const auto PSD_histo = global_status.histos_PSD[channel];
        const Json::Value PSD_config = PSD_histo.config;

        PSD_configs[std::to_string(channel)] = PSD_config;
    }

    Json::Value configs;

    configs["qlong"] = qlong_configs;
    configs["PSD"] = PSD_configs;

    Json::Value status_message;

    status_message["configs"] = configs;
    status_message["active_channels"] = active_channels;

    actions::generic::publish_status(global_status, defaults_pqrs_status_topic, status_message);

    return states::RECEIVE_COMMANDS;
}

state actions::receive_commands(status &global_status)
{
    void *commands_socket = global_status.commands_socket;

    Json::Value json_message = socket_functions::receive_json_message(commands_socket);

    if (json_message.isMember("command"))
    {
        std::string command;
        try
        {
            command = json_message["command"].asString();
        }
        catch (...) { };

        if (command == std::string("reset") && json_message.isMember("arguments"))
        {
            const Json::Value arguments = json_message["arguments"];

            if (arguments.isMember("channel") && arguments.isMember("type"))
            {
                std::string channel;
                std::string type;

                try
                {
                    type = arguments["type"].asString();
                    channel = arguments["channel"].asString();
                }
                catch (...) { }

                Json::Value event_message;

                event_message["type"] = "event";
                event_message["event"] = "Reset of histo type: " + type + ", channel: " + channel;

                if (global_status.verbosity > 0)
                {
                    std::cout << "[" << utilities_functions::time_string() << "] ";
                    std::cout << "Received command: " << command << "; ";
                    std::cout << "Channel: " << channel << "; ";
                    std::cout << "Type: " << type << "; ";
                    std::cout << std::endl;
                }

                if (channel == std::string("all"))
                {
                    for (const unsigned int &channel: global_status.active_channels)
                    {
                        try
                        {
                            // A reset is fine for all types of histograms
                            if (type == std::string("qlong") || type == std::string("all"))
                            {
                                global_status.histos_qlong[channel].reset();
                            }
                            if (type == std::string("PSD") || type == std::string("all"))
                            {
                                global_status.histos_PSD[channel].reset();
                            }
                        }
                        //catch (const std::out_of_range& oor) { }
                        catch (...) { }
                    }

                    actions::generic::publish_status(global_status, defaults_pqrs_events_topic, event_message);
                }
                else
                {
                    try
                    {
                        unsigned int int_channel = arguments["channel"].asUInt();

                        if (int_channel == std::stoul(channel))
                        {
                            if (type == std::string("qlong") || type == std::string("all"))
                            {
                                global_status.histos_qlong[int_channel].reset();
                            }
                            if (type == std::string("PSD") || type == std::string("all"))
                            {
                                global_status.histos_PSD[int_channel].reset();
                            }

                            actions::generic::publish_status(global_status, defaults_pqrs_events_topic, event_message);
                        }
                    }
                    //catch (const Json::LogicError& le) { }
                    //catch (const std::out_of_range& oor) { }
                    //catch (const std::invalid_argument& ia) { }
                    catch (...) { }
                }
            }
        }
        else if (command == std::string("reconfigure") && json_message.isMember("arguments"))
        {
            const Json::Value arguments = json_message["arguments"];

            if (arguments.isMember("channel") && arguments.isMember("config") && arguments.isMember("type"))
            {
                std::string channel;
                std::string type;

                const Json::Value new_config = arguments["config"];

                try
                {
                    type = arguments["type"].asString();
                    channel = arguments["channel"].asString();
                }
                catch (...) { }

                Json::Value event_message;

                event_message["type"] = "event";
                event_message["event"] = "Reconfiguration of histo type: " + type + ", channel: " + channel;

                if (global_status.verbosity > 0)
                {
                    std::cout << "[" << utilities_functions::time_string() << "] ";
                    std::cout << "Received command: " << command << "; ";
                    std::cout << "Channel: " << channel << "; ";
                    std::cout << "Type: " << type << "; ";
                    std::cout << std::endl;
                }

                if (channel == std::string("all"))
                {
                    for (const unsigned int &channel: global_status.active_channels)
                    {
                        try
                        {
                            // A reset is fine for all types of histograms
                            // a reconfiguration requires different parameters
                            if (type == std::string("qlong"))
                            {
                                global_status.histos_qlong[channel].reconfigure(new_config);
                            }
                            else if (type == std::string("PSD"))
                            {
                                global_status.histos_PSD[channel].reconfigure(new_config);
                            }
                        }
                        //catch (const std::out_of_range& oor) { }
                        catch (...) { }
                    }

                    actions::generic::publish_status(global_status, defaults_pqrs_events_topic, event_message);
                }
                else
                {
                    try
                    {
                        const unsigned int int_channel = arguments["channel"].asUInt();

                        // This is to check if the number was read correctly
                        // on ubuntu 14 it does not work!
                        //if (int_channel == std::stoul(channel))
                        {
                            if (type == std::string("qlong"))
                            {
                                global_status.histos_qlong[int_channel].reconfigure(new_config);
                            }
                            else if (type == std::string("PSD"))
                            {
                                global_status.histos_PSD[int_channel].reconfigure(new_config);
                            }

                            event_message["event"] = "Reconfiguration of histo type: " + type + ", channel: " + std::to_string(int_channel);
                            actions::generic::publish_status(global_status, defaults_pqrs_events_topic, event_message);
                        }
                    }
                    //catch (const Json::LogicError& le) { }
                    //catch (const std::out_of_range& oor) { }
                    //catch (const std::invalid_argument& ia) { }
                    catch (...) { }
                }
            }
        }
        else if (command == std::string("clear"))
        {
            global_status.active_channels.clear();
            global_status.histos_qlong.clear();
            global_status.histos_PSD.clear();
        }
        else if (command == std::string("quit"))
        {
            return states::CLOSE_SOCKETS;
        }
    }

    return states::READ_SOCKET;
}

state actions::read_socket(status &global_status)
{
    void *abcd_data_socket = global_status.abcd_data_socket;

    std::vector<char> message = socket_functions::receive_byte_message(abcd_data_socket);

    while (message.size() > 0)
    {
        if (global_status.verbosity > 0)
        {
            std::cout << "[" << utilities_functions::time_string() << "] ";
            std::cout << "Message size: " << message.size() << "; ";
            std::cout << std::endl;
        }

        char *char_buffer = reinterpret_cast<char*>(message.data());

        const char *position = strchr(char_buffer, ' ');
        const size_t topic_length = position - char_buffer;

        std::string topic(char_buffer, topic_length);

        if (global_status.verbosity > 0)
        {
            std::cout << "[" << utilities_functions::time_string() << "] ";
            std::cout << "Topic: " << topic << "; ";
            std::cout << std::endl;
        }

        if (topic.compare(0, strlen(defaults_abcd_data_events_topic), defaults_abcd_data_events_topic) == 0)
        {
            const clock_t event_start = clock();

            const size_t data_size = message.size() - topic.size() - 1;

            const size_t events_number = data_size / sizeof(event_PSD);

            if (global_status.verbosity > 0)
            {
                std::cout << "[" << utilities_functions::time_string() << "] ";
                std::cout << "Data size: " << data_size << "; ";
                std::cout << "Events number: " << events_number << "; ";
                std::cout << "mod: " << data_size % sizeof(event_PSD) << "; ";
                std::cout << std::endl;
            }

            const event_PSD *events = reinterpret_cast<const event_PSD*>(position + 1);
            for (size_t i = 0; i < events_number; i++)
            {
                const event_PSD event = events[i];

                const unsigned int channel = event.channel;

                const auto it = std::find(global_status.active_channels.begin(),
                                          global_status.active_channels.end(),
                                          channel);
                if (it == global_status.active_channels.end())
                {
                    global_status.active_channels.push_back(channel);
                    std::sort(global_status.active_channels.begin(),
                              global_status.active_channels.end());

                    global_status.histos_qlong[channel] = histogram<unsigned int>
                                                                   (defaults_pqrs_bins_E,
                                                                    defaults_pqrs_min_E,
                                                                    defaults_pqrs_max_E,
                                                                    global_status.verbosity);

                    global_status.histos_PSD[channel] = histogram2D<unsigned int, double>
                                                                   (defaults_pqrs_bins_E,
                                                                    defaults_pqrs_min_E,
                                                                    defaults_pqrs_max_E,
                                                                    defaults_pqrs_bins_PSD,
                                                                    defaults_pqrs_min_PSD,
                                                                    defaults_pqrs_max_PSD,
                                                                    global_status.verbosity);
                }


                const unsigned int qshort = event.qshort;
                const unsigned int qlong = event.qlong;
                const double psd = static_cast<double>(qlong - qshort) / qlong;

                if (global_status.verbosity > 1)
                {
                    std::cout << "[" << utilities_functions::time_string() << "] ";
                    std::cout << "Event: " << i << "; ";
                    std::cout << "Channel: " << channel << "; ";
                    std::cout << "qshort: " << qshort << "; ";
                    std::cout << "qlong: " << qlong << "; ";
                    std::cout << "PSD: " << psd << "; ";
                    std::cout << std::endl;
                }

                global_status.histos_qlong[channel].fill(qlong);
                global_status.histos_PSD[channel].fill(qlong, psd);
            }

            const clock_t event_stop = clock();

            if (global_status.verbosity > 0)
            {
                const float elaboration_time = (float)(event_stop - event_start) / CLOCKS_PER_SEC * 1000;
                const float elaboration_speed = data_size / elaboration_time * 1000.0 / 1024.0 / 1024.0;

                std::cout << "[" << utilities_functions::time_string() << "] ";
                std::cout << "size: " << data_size << "; ";
                std::cout << "events_number: " << events_number << "; ";
                std::cout << "elaboration_time: " << elaboration_time << " ms; ";
                std::cout << "elaboration_speed: " << elaboration_speed << " MBi/s; ";
                std::cout << std::endl;
            }
        }

        message = socket_functions::receive_byte_message(abcd_data_socket);
    }

    const std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
    if (now - global_status.last_publication > std::chrono::seconds(defaults_pqrs_publish_timeout))
    {
        return states::PUBLISH_DATA;
    }

    return states::READ_SOCKET;
}

state actions::publish_data(status &global_status)
{
    Json::Value active_channels;
    Json::Value qlong_data;
    Json::Value PSD_data;

    for (const unsigned int &channel: global_status.active_channels)
    {
        active_channels.append(channel);

        const Json::Value qlong_histo = global_status.histos_qlong[channel].to_json();

        qlong_data[std::to_string(channel)] = qlong_histo;

        const Json::Value PSD_histo = global_status.histos_PSD[channel].to_json();

        PSD_data[std::to_string(channel)] = PSD_histo;
    }

    Json::Value data;

    data["qlong"] = qlong_data;
    data["PSD"] = PSD_data;

    const unsigned long int data_msg_ID = global_status.data_msg_ID;

    Json::Value status_message;

    status_message["module"] = "pqrs";
    status_message["timestamp"] = utilities_functions::time_string();
    status_message["msg_ID"] = Json::Value::UInt64(data_msg_ID);
    status_message["data"] = data;
    status_message["active_channels"] = active_channels;

    void *data_socket = global_status.data_socket;

    socket_functions::send_json_message(data_socket, defaults_pqrs_data_histograms_topic, status_message, 0);

    global_status.data_msg_ID += 1;

    return states::PUBLISH_STATUS;
}

state actions::close_sockets(status &global_status)
{
    void *status_socket = global_status.status_socket;
    void *data_socket = global_status.data_socket;
    void *commands_socket = global_status.commands_socket;
    void *abcd_data_socket = global_status.abcd_data_socket;

    const int s = zmq_close(status_socket);
    if (s != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on status socket close: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    const int d = zmq_close(data_socket);
    if (d != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on data socket close: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    const int c = zmq_close(commands_socket);
    if (c != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on commands socket close: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    const int a = zmq_close(abcd_data_socket);
    if (a != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on abcd data socket close: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    return states::DESTROY_CONTEXT;
}

state actions::destroy_context(status &global_status)
{
    void *context = global_status.context;

    const int c = zmq_ctx_destroy(context);
    if (c != 0)
    {
        std::cout << '[' << utilities_functions::time_string() << "] ";
        std::cout << "ZeroMQ Error on context destroy: ";
        std::cout << zmq_strerror(errno);
        std::cout << std::endl;
    }

    return states::STOP;
}

state actions::communication_error(status &global_status)
{
    Json::Value status_message;

    status_message["type"] = "error";
    status_message["error"] = "Communication error";

    actions::generic::publish_status(global_status, defaults_pqrs_events_topic, status_message);

    return states::CLOSE_SOCKETS;
}

state actions::stop(status&)
{
    return states::STOP;
}
