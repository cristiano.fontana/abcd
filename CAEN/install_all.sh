#! /bin/bash

machine_type=`uname -m`
root_dir="`pwd`"
libraries="CAENVME CAENComm CAENDigitizer CAENDPP"

for lib in $libraries
do
    echo "Installing: ""$lib"

    cd "$lib"
    mkdir temp

    last_release="`ls "$lib"*.tgz | sort -V | tail -n 1`"

    echo "Last release: ""$last_release"

    tar -C temp -xzf "$last_release"

    if [ "${machine_type}" == 'x86_64' ]
    then
        installer="`find temp -iname 'install*' | grep 64`"

        if [ "x""$installer""x" == "xx" ]
        then
            installer="`find temp -iname 'install*'`"
        else
            other_installer="`dirname "$installer"`""/install"
            echo "Deleting 32 bit installer: ""$other_installer"
            rm "$other_installer"
        fi

        echo "Installer: ""$installer"

        cd "`dirname "$installer"`"

        echo "Executing: ""$installer"
        sh "`basename "$installer"`"
        echo "Done!"
    else
        echo "Warning 32 bit machine!!!"

        installer="`find temp -iname 'install*' | grep -v 64`"

        echo "Installer: ""$installer"

        cd "`dirname "$installer"`"

        echo "Executing: ""$installer"
        sh "`basename "$installer"`"
        echo "Done!"
    fi

    echo "Cleaning up"
    cd "$root_dir""/""$lib"

    rm -rf temp

    cd "$root_dir"
done


libraries="CAENUSBdrv A3818"

for lib in $libraries
do
    echo "Installing: ""$lib"

    cd "$lib"
    mkdir temp

    last_release="`ls "$lib"*.tgz | sort -V | tail -n 1`"

    echo "Last release: ""$last_release"

    tar -C temp -xzf "$last_release"

    if [ "${lib}" == A3818 ]
    then
	# No need to patch anymore
	pwd
	#a3818_source="`find . -iname 'a3818.c' | tail -n 1`"
	#echo "Patching A3818 source: '${a3818_source}'"
	#patch ${a3818_source} a3818.diff

	#a3818_installer="`find . -iname 'install_64.sh' | tail -n 1`"
	#echo "Patching A3818 installer: '${a3818_installer}'"
	#patch ${a3818_installer} install_64.diff
    elif [ "${lib}" == CAENUSBdrv ]
    then
	pwd
	CAENUSBdrv_source="`find . -iname 'CAENUSBdrvB.c' | tail -n 1`"

	# No need to patch anymore
	#echo "Patching CAENUSBdrv source: '${CAENUSBdrv_source}'"
	#patch ${CAENUSBdrv_source} CAENUSBdrv.diff
    fi

    makefile="`find temp -iname 'Makefile'`"

    echo "Makefile: ""$makefile"

    cd "`dirname "$makefile"`"

    echo "Compiling"
    make

    echo "Installing"
    make install

    echo "Cleaning up"
    cd "$root_dir""/""$lib"

    rm -rf temp

    cd "$root_dir"
done
